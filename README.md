# COVID-19 AutoDérogations [OUTDATED]
## En raison de la mise à jour du 24 mars de la nouvelle fiche, le  fichier .pdf étant au format protégé, et aux applications similaires existantes de nature malhonnête récupérant vos données, ce projet à but pédagogique ne sera pas mis à jour.
### Outil de remplissage automatique de dérogation durant le confinement français du COVID-19.

Cet outil est un formulaire de remplissage automatique pour 6 jours, à renouveler. Les informations à renseigner sont les mêmes demandées sur le fichier d'origine, trouvable sur le site du gouvernement.\
Aucune donnée personnelle n'est réutilisée  où que ce soit, tout se fait directement.\
Il utilise les lib PHP [*FPDF*](http://www.fpdf.org/) et [*FPDI*](https://www.setasign.com/products/fpdi/about/).

## ~~À essayer **ici**!~~