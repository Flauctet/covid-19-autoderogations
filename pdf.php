<?php

use setasign\Fpdi\Fpdi;

require_once('fpdf.php');
require_once('autoload.php');
header('charset=utf-8');

$tj = gmdate("j", time() + 3600); //Jour actuel
$tm = gmdate("m", time() + 3600); //Mois actuel
$data = $_REQUEST;

/**
 * La variable table contient tous les caractères spéciaux ne pouvant pas rentrer dans le PDF sans rendre illisible (par un humain) le dossier
 */
$table = array(
    'Š' => 'S', 'š' => 's', 'Ð' => 'Dj', 'd' => 'dj', 'Ž' => 'Z', 'ž' => 'z', 'C' => 'C', 'c' => 'c', 'C' => 'C', 'c' => 'c',
    'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E',
    'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O',
    'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss',
    'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e',
    'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o',
    'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'ý' => 'y', 'þ' => 'b',
    'ÿ' => 'y', 'R' => 'R', 'r' => 'r',
);

$data["city"] = strtr($data["city"], $table); //Remplace les caractères spéciaux dans le nom de la ville
$data["address"] = strtr($data["address"], $table); //Remplace les caractères spéciaux dans l'adresse
$data["identity"] = strtr($data["identity"], $table); //Remplace les caractères spéciaux dans le nom et le prénom

$pdf = new Fpdi(); //Créer un nouveau fichier PDF

/**
 * Nous voulons créer 6 pages.
 * Pour chaque page, nous rentrerons les mêmes valeurs (nom, date de naissance, domicile, raisons) mais
 * nous changerons la date.
 */
for ($i = 0; $i < $data['pages']; $i++) {
    $pdf->AddPage(); //Ajoute une page
    $pdf->setSourceFile('files/attestation_de_deplacement_derogatoire.pdf'); //Le fichier source, disponible sur le site du gouvernement
    // import page 1 du pdf d'origine
    $tplIdx = $pdf->importPage(1);
    //on copie tous les éléments de celui-ci, sauf les cases à cocher
    $pdf->useTemplate($tplIdx);

    //Nous utiliserons la police Helvetica et la couleur noir
    $pdf->SetFont('Helvetica', '', '13');
    $pdf->SetTextColor(0, 0, 0);

    //Nom + position sur le fichier
    $pdf->SetXY(45, 75);
    $pdf->Write(0, strtoupper($data["identity"]));

    //Date de naissance + position sur le fichier 
    $pdf->SetXY(45, 85);
    $pdf->Write(0, date('d/m/Y', strtotime($data["date"])));

    //Adresse + position sur le fichier
    $pdf->SetXY(45, 97);
    $pdf->Write(0, strtoupper($data["address"]));
    $pdf->SetXY(45, 104);
    $pdf->Write(0, strtoupper($data["city"]));

    //Raisons + position sur le fichier
    $pdf->SetFont('ZapfDingbats', '', 16); //Cette police contourne actuellement les checkmark en UTF-8, non disponible dans mon fichier PDF.
    if ($data['raison'] == "raison1") {
        $pdf->SetXY(20, 156); //Travail
        $pdf->Write(0, 4);
    }
    if ($data['raison'] == "raison2") {
        $pdf->SetXY(20, 176); //Courses
        $pdf->Write(0, 4);
    }
    if ($data['raison'] == "raison3") {
        $pdf->SetXY(20, 189); //Santé
        $pdf->Write(0, 4);
    }
    if ($data['raison'] == "raison4") {
        $pdf->SetXY(20, 202); //Assistance/garde
        $pdf->Write(0, 4);
    }
    if ($data['raison'] == "raison5") {
        $pdf->SetXY(20, 220); //Sortie sport/chien
        $pdf->Write(0, 4);
    }

    //Date/Fait à + position sur le fichier
    $pdf->SetFont('Helvetica', '', '13'); //retour en Helvetica
    $pdf->SetXY(130, 246);
    $pdf->Write(0, substr(ucwords($data['city'], "-"), 0, 10)); //Fait à = On coupe le nom de la ville à 10 caractère, pour éviter de dépasser sur la date
    if (((int) $tj) > 30) { //Si le jour actuel dépasse le 30 (à optimiser)
        if (
            (int) $tm == 1 ||
            (int) $tm == 3 ||
            (int) $tm == 5 ||
            (int) $tm == 7 ||
            (int) $tm == 8 ||
            (int) $tm == 10 ||
            (int) $tm == 12
        ) { //Si le mois actuel est un mois qui a 31 jours
            if (((int) $tj) > 31) { //si le jour actuel dépasse 31 alors on change la date au 1 du mois suivant
                $tj = "01";
                if ((int) $tm < 9) $tm = "0" . strval((int) $tm + 1);
                else $tm = strval((int) $tm + 1); //ici je voulais juste rajouter un 0 devant le numéro du mois, pour faire + beau
            }
        } else if (
            (int) $tm == 4 ||
            (int) $tm == 6 ||
            (int) $tm == 9 ||
            (int) $tm == 11
        ) { //sinon si le mois actuel est un mois qui a 30 jours alors on change la date au 1 du mois suivant 
            $tj = "01";
            if ((int) $tm < 9) $tm = "0" . strval((int) $tm + 1);
            else $tm = strval((int) $tm + 1);
        } else if ((int) $tm == 2) { //sinon si nous sommes en février (je ne l'espère pas), alors on change la date au 1 du mois de mars (mois suivant)
            $tj = "01";
            $tm = "03";
        }
    }
    $pdf->SetXY(168, 246); //Position jour
    $pdf->Write(0, $tj);
    $pdf->SetXY(176, 246); //position mois
    $pdf->Write(0, $tm);
    if ((int) $tj < 9) $tj = "0" . strval((int) $tj + 1);
    else $tj = strval((int) $tj + 1); //ici je voulais juste rajouter un 0 devant le numéro du mois, pour faire + beau
}



$pdf->Output('attestation_de_deplacement_derogatoire_auto.pdf', 'I'); //retourne le fichier .pdf de 6 pages en présentation sur le navigateur
